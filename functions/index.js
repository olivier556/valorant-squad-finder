const cors = require('cors')({origin: true});

const functions = require('firebase-functions');

const admin = require('firebase-admin');
admin.initializeApp();

exports.offlineCheck = functions.database.ref('/status/{uid}')
  .onUpdate((snapshot, context) => {
    console.log('UPDATING')
    
    if (snapshot.after._data.online === false) {
      console.log('GOING OFFLINE')
    } else {
      console.log('GOING ONLINE')
    }
    // Grab the current value of what was written to the Realtime Database.
    return deleteUser(context.auth.uid, snapshot.after._data.lobbyId);
  });

// // Create and Deploy Your First Cloud Functions
// // https://firebase.google.com/docs/functions/write-firebase-functions
//
exports.helloWorld = functions.https.onRequest( async (request, response) => {
  cors(request, response, () => {
    functions.logger.info("Hello logs!", {structuredData: true});
    response.send("Hello from Firebase!");
  });
});

async function deleteUser(uid, lobbyId) {
  const batch = admin.firestore().batch()

  const snapshot = await admin.firestore().collection('lobbies')
    .where('host', '=', uid).get()

  snapshot.docs.forEach(doc => {
    batch.delete(doc.ref);
  });

  await batch.commit();

  if (lobbyId) {
    await admin
      .firestore()
      .collection("lobbies")
      .doc(`${lobbyId}`)
      .update(`players.${uid}`, admin.firestore.FieldValue.delete())
  }

  // await admin
  //   .firestore()
  //   .collection("lobbies")
  //   .where(`players.${uid}`, '>', '')
  //   .update(`players.${uid}`, admin.firestore.FieldValue.delete())
}

async function cleanUpPlayers() {
  const batch = admin.firestore().batch()

  const snapshot = await admin.firestore().collection('players')
    .where('lastSeen', '<', Date.now() - (30 * 1000)).get()

  snapshot.docs.forEach((doc) => {
    deleteUser(doc.data().uid)
    batch.delete(doc.ref);
  });
  await batch.commit();
  console.log('all done');
}
cleanUpPlayers();
exports.offlineCleanup = functions.pubsub.schedule('every 1 minutes').onRun((context) => {
  console.log('offline cleanup')
  return cleanUpPlayers();
});

exports.join = functions.https.onRequest(async (request, response) => {
  cors(request, response, async () => {
    const token = request.body.data.token;
    const lobbyId = request.body.data.lobby;
    const rank = request.body.data.rank;
    const valorant = request.body.data.valorant;
    functions.logger.info(token);

    const decodedToken = await admin.auth().verifyIdToken(token);

    const player = {
      rank,
      valorant,
      uid: decodedToken.uid,
      timeJoined: Date.now()
    };

    admin
      .firestore()
      .collection("lobbies")
      .doc(`${lobbyId}`)
      .update(`players.${decodedToken.uid}`, player);

    response.send({data:{}});
  });
});

exports.create = functions.https.onRequest(async (request, response) => {
  cors(request, response, async () => {
    const token = request.body.data.token;
    const name = request.body.data.name;
    const rank = request.body.data.rank;
    const valorant = request.body.data.valorant;
    functions.logger.info(token);

    const decodedToken = await admin.auth().verifyIdToken(token);

    const player = {
      rank,
      valorant,
      uid: decodedToken.uid,
      timeJoined: Date.now()
    };

    const lobby = {
      name,
      host: decodedToken.uid,
      timeCreated: Date.now(),
      players: {}
    };

    lobby.players[decodedToken.uid] = player;

    const ref = await admin
      .firestore()
      .collection("lobbies")
      .doc();

    const refId = ref.id;

    ref.set(lobby);

    response.send({
      data: {
        lobbyId: refId
      }
    });
  })
})

  // Take the text parameter passed to this HTTP endpoint and insert it into 
  // Cloud Firestore under the path /messages/:documentId/original
  // exports.addMessage = functions.https.onRequest(async (req, res) => {
  //   // Grab the text parameter.
  //   const original = req.query.text;
  //   // Push the new message into Cloud Firestore using the Firebase Admin SDK.
  //   const writeResult = await admin.firestore().collection('messages').add({original: original});
  //   // Send back a message that we've succesfully written the message
  //   res.json({result: `Message with ID: ${writeResult.id} added.`});
  // });

  exports.leave = functions.https.onRequest(async (request, response) => {
    cors(request, response, async () => {
      const token = request.body.data.token;
      const lobbyId = request.body.data.lobby;
      const target = request.body.data.target;
      const decodedToken = await admin.auth().verifyIdToken(token);

      const lobby = await admin.firestore()
        .collection("lobbies")
        .doc(lobbyId)
        .get()

      if (!lobby.exists) {
        response.sendStatus(200);
        return;
      }

      if (target) {
        if (lobby.data().host === decodedToken.uid) {
          if (decodedToken.uid !== target) {
            await admin
              .firestore()
              .collection("lobbies")
              .doc(`${lobbyId}`)
              .update(`players.${target}`, admin.firestore.FieldValue.delete())
            response.sendStatus(200);
            return;
          }
        } else {
          response.sendStatus(401);
          return;
        }
      }

      if (lobby.data().host === decodedToken.uid) {
        console.log('IS HOST')
        await admin
          .firestore()
          .collection("lobbies")
          .doc(`${lobbyId}`)
          .delete()
      } else {
        console.log('IS NOT HOST')
        await admin
          .firestore()
          .collection("lobbies")
          .doc(`${lobbyId}`)
          .update(`players.${decodedToken.uid}`, admin.firestore.FieldValue.delete())
      }

      response.sendStatus(200);
    });
  });
