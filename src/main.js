import Vue from "vue";
import App from "./App.vue";
import router from "./router";
import { firestorePlugin } from "vuefire";
import ElementUI from 'element-ui'
import "./main.scss";
// import { db } from './util/db';
// import ranks from './util/ranks';

const serialize = (snapshot) => {
  return {...snapshot.data(), id: snapshot.id }
}

// const data = {
//   valorant: 'chokemaster#2344',
//   rank: ranks.gold3
// };

// // Add a new document in collection "cities" with ID 'LA'
// db.collection('players').doc().set(data);


Vue.use(ElementUI);
Vue.use(firestorePlugin, { serialize });

Vue.config.productionTip = false;

new Vue({
  router,
  render: h => h(App)
}).$mount("#app");
