import Vue from "vue";
import VueRouter from "vue-router";
import Home from "../views/Home.vue";
import LobbyForm from "../views/LobbyForm.vue";
import Lobby from "../views/Lobby.vue";

Vue.use(VueRouter);

const routes = [
  {
    path: "/",
    name: "Home",
    component: Home
  },
  {
    path: "/create-lobby",
    name: "Create Lobby",
    component: LobbyForm
  },
  {
    path: "/lobby/:id",
    name: "Lobby",
    component: Lobby
  },
  {
    path: "/about",
    name: "About",
    // route level code-splitting
    // this generates a separate chunk (about.[hash].js) for this route
    // which is lazy-loaded when the route is visited.
    component: () =>
      import(/* webpackChunkName: "about" */ "../views/About.vue")
  }
];

const router = new VueRouter({
  routes
});

export default router;
