import firebase from "firebase/app";
import "firebase/firestore";
import "firebase/database";
import "firebase/auth";
import "firebase/functions";

const isDev = process.env.NODE_ENV === "development"

// Get a Firestore instance
export const instance = firebase.initializeApp({
  projectId: "squad-finder-916af",
  apiKey: "AIzaSyDdFmHmCcc3lwi6oPFcO_I5k3Mq3XVg0F8",
  // databaseURL: isDev ? "http://localhost:9000/?ns=squad-finder-916af" : "https://squad-finder-916af.firebaseio.com/"
  databaseURL: "https://squad-finder-916af.firebaseio.com/"
});

export const db = instance.firestore();
export const auth = instance.auth();
export const functions = instance.functions();
export const oldDb = instance.database();

if (isDev) {
  functions.useFunctionsEmulator("http://localhost:5001");
}

// Export types that exists in Firestore
// This is not always necessary, but it's used in other examples
const { Lobbies } = firebase.firestore;
export { Lobbies };
