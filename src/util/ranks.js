export default {
  IRON1: {
    label: "Iron 1",
    key: "IRON1",
    imageSrc: require("../assets/rank-iron1.png")
  },
  IRON2: {
    label: "Iron 2",
    key: "IRON2",
    imageSrc: require("../assets/rank-iron2.png")
  },
  IRON3: {
    label: "Iron 3",
    key: "IRON3",
    imageSrc: require("../assets/rank-iron3.png")
  },
  BRONZE1: {
    label: "Bronze 1",
    key: "BRONZE1",
    imageSrc: require("../assets/rank-bronze1.png")
  },
  BRONZE2: {
    label: "Bronze 2",
    key: "BRONZE2",
    imageSrc: require("../assets/rank-bronze2.png")
  },
  BRONZE3: {
    label: "Bronze 3",
    key: "BRONZE3",
    imageSrc: require("../assets/rank-bronze3.png")
  },
  SILVER1: {
    label: "Silver 1",
    key: "SILVER1",
    imageSrc: require("../assets/rank-silver1.png")
  },
  SILVER2: {
    label: "Silver 2",
    key: "SILVER2",
    imageSrc: require("../assets/rank-silver2.png")
  },
  SILVER3: {
    label: "Silver 3",
    key: "SILVER3",
    imageSrc: require("../assets/rank-silver3.png")
  },
  GOLD1: {
    label: "Goldd 1",
    key: "GOLD1",
    imageSrc: require("../assets/rank-gold1.png")
  },
  GOLD2: {
    label: "Gold 2",
    key: "GOLD2",
    imageSrc: require("../assets/rank-gold2.png")
  },
  GOLD3: {
    label: "Gold 3",
    key: "GOLD3",
    imageSrc: require("../assets/rank-gold3.png")
  },
  PLATINUM1: {
    label: "Platinum 1",
    key: "PLATINUM1",
    imageSrc: require("../assets/rank-platinum1.png")
  },
  PLATINUM2: {
    label: "Platinum 2",
    key: "PLATINUM2",
    imageSrc: require("../assets/rank-platinum2.png")
  },
  PLATINUM3: {
    label: "Platinum 3",
    key: "PLATINUM3",
    imageSrc: require("../assets/rank-platinum3.png")
  },
  DIAMOND1: {
    label: "Diamond 1",
    key: "DIAMOND1",
    imageSrc: require("../assets/rank-diamond1.png")
  },
  DIAMOND2: {
    label: "Diamond 2",
    key: "DIAMOND2",
    imageSrc: require("../assets/rank-diamond2.png")
  },
  DIAMOND3: {
    label: "Diamond 3",
    key: "DIAMOND3",
    imageSrc: require("../assets/rank-diamond3.png")
  },
  IMMORTAL1: {
    label: "Immortal 1",
    key: "IMMORTAL1",
    imageSrc: require("../assets/rank-immortal1.png")
  },
  IMMORTAL2: {
    label: "Immortal 2",
    key: "IMMORTAL2",
    imageSrc: require("../assets/rank-immortal2.png")
  },
  IMMORTAL3: {
    label: "Immortal 3",
    key: "IMMORTAL3",
    imageSrc: require("../assets/rank-immortal3.png")
  },
  RADIANT: {
    label: "Radiant",
    key: "RADIANT",
    imageSrc: require("../assets/rank-radiant.png")
  }
};
