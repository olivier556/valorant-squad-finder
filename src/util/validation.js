export const validateValName = (rule, value, callback) => {
  if (value.indexOf('#') === -1) {
    callback(new Error('Done forget to add the last 4 digits: i.e. "#2222"'));
  }
  callback();
};
